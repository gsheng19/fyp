package util

import (
	"database/sql"
	"encoding/json"
	"strconv"

	_ "github.com/go-sql-driver/mysql"
	log "github.com/sirupsen/logrus"
	"gitlab.com/gsheng19/fyp/worker-mcq/models"
)

func ConnectMySQL() *sql.DB {
	username := GetEnvDefault("MYSQL_USER", "")
	password := GetEnvDefault("MYSQL_PASSWORD", "")
	port := GetEnvDefault("MYSQL_PORT", "3306")
	host := GetEnvDefault("MYSQL_HOST", "localhost")
	name := GetEnvDefault("MYSQL_DATABASE", "aasp")

	db, err := sql.Open("mysql", username+":"+password+"@tcp("+host+":"+port+")/"+name)
	if err != nil {
		panic(err)
	}

	// Open doesn't open a connection. Validate DSN data:
	err = db.Ping()
	if err != nil {
		panic(err.Error())
	}

	return db
}

// Get Submission attempt from DB and push to Redis
func GetSubmissionAttemptAsRedis(id string) (*models.UserAnswer, []models.ActualAnswers) {
	db := ConnectMySQL()
	defer db.Close()

	status := models.UserAnswer{AnswerText: ""}
	stmtGen, err := db.Prepare("SELECT qq.attemptid, qn.questionid, qq.answer, qn.id FROM quiz_attempt_qn qq JOIN quiz_questions qn ON qq.questionid=qn.id WHERE qq.id=?")
	if err != nil {
		log.Errorln(err.Error())
		return nil, nil
	}
	defer stmtGen.Close()
	var aid int
	var qid int
	var qnid int

	err = stmtGen.QueryRow(id).Scan(&aid, &qid, &status.AnswerText, &qnid)
	log.Warnln("hello!")
	//status.ResponseLowered = strings.ToLower(status.AnswerText)
	if err != nil {
		log.Errorln(err.Error())
		return nil, nil
	}

	// Get max score
	stmtScore, err := db.Prepare("SELECT q.maxscore FROM quiz_questions qn JOIN question q ON qn.questionid=q.id WHERE qn.id=?")
	if err != nil {
		log.Errorln(err.Error())
		return nil, nil
	}
	defer stmtScore.Close()
	var maxScore sql.NullString
	err = stmtScore.QueryRow(qnid).Scan(&maxScore)
	if err != nil {
		log.Errorln(err.Error())
		return nil, nil
	}
	if maxScore.Valid {
		status.MaxScoreQn = json.Number(maxScore.String)
	} else {
		status.MaxScoreQn = "0"
	}

	// Get question count  ## mcq dont need... for now...
	// keyCnt, err := db.Prepare("SELECT length(id) FROM question_structured_keywords WHERE questionid=?")
	// if err != nil {
	// 	log.Errorln(err.Error())
	// 	return nil, nil
	// }
	// defer keyCnt.Close()
	// var qCount int
	// err = keyCnt.QueryRow(qid).Scan(&qCount)
	// if err != nil {
	// 	log.Errorln(err.Error())
	// 	return nil, nil
	// }

	mScore, _ := status.MaxScoreQn.Float64()
	//log.Debugf("Max Score: %f | Qn Cnt: %d", mScore, qCount)
	log.Debugf("Max Score: %f ", mScore)

	// Get keywords
	correctansRows, err := db.Query("SELECT answer,iscorrect,rationale,score FROM question_mcq_qns WHERE questionid=? AND iscorrect=?", qid, 1)
	if err != nil {
		log.Errorln(err.Error())
		return &status, nil
	}
	defer correctansRows.Close()

	var correctAns []models.ActualAnswers

	for correctansRows.Next() {
		var kw models.ActualAnswers
		var isCorrectInt int
		var rationale string
		var score sql.NullString
		err = correctansRows.Scan(&kw.Answer, &rationale, &isCorrectInt, &score)
		if err != nil {
			log.Errorln(err.Error())
			continue
		}

		if score.Valid {
			kw.Score = json.Number(score.String)
		} else {
			// divide and round up
			kw.Score = json.Number(strconv.FormatFloat(mScore, 'e', 2, 64))
			log.Debugln("Score split assigned to keyword as " + kw.Score)
		}
		kw.Correct = isCorrectInt == 1

		correctAns = append(correctAns, kw) // Add to keywords
	}

	return &status, correctAns
}

// Update Submission Attempt in DB for structured questions
func UpdateSubmissionAttempt(id string, score float64) bool {
	db := ConnectMySQL()
	defer db.Close()

	stmtMaxScore, err := db.Prepare("SELECT qn.maxscore FROM question qn JOIN quiz_questions qq ON qq.questionid = qn.id JOIN quiz_attempt_qn qa ON qa.questionid = qq.id WHERE qa.id=?")
	if err != nil {
		log.Errorln(err.Error())
		return false
	}
	defer stmtMaxScore.Close()

	stmtUpdateTestAttempt, err := db.Prepare("UPDATE quiz_attempt_qn SET score=?, ismarked=1 WHERE id=?")
	if err != nil {
		log.Errorln(err.Error())
		return false
	}
	defer stmtUpdateTestAttempt.Close()

	var maxscore json.Number
	err = stmtMaxScore.QueryRow(id).Scan(&maxscore)
	if err != nil {
		log.Errorln(err.Error())
		return false
	}
	maxscoreFloat, _ := maxscore.Float64()

	if score > maxscoreFloat {
		score = maxscoreFloat
	}

	log.Debugf("Score: %f | Submit: %f", maxscoreFloat, score)

	_, err = stmtUpdateTestAttempt.Exec(score, id)
	if err != nil {
		log.Errorln(err.Error())
		return false
	}
	return true
}
