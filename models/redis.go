package models

import "encoding/json"

type QueueObj struct {
	Key  string `json:"key"`
	Type string `json:"type"`
}

type UserAnswer struct {
	AnswerText string `json:"answertext"`
	//ResponseLowered string      `json:"response_lowercase"`
	MaxScoreQn json.Number `json:"max_score"`
}

type ActualAnswers struct {
	Answer    string      `json:"answer"`
	Correct   bool        `json:"isCorrect"`
	Rationale string      `json:"rationale"`
	Score     json.Number `json:"score"`
}
